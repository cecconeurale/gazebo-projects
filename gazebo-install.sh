#!/bin/bash

# Copyright (C) 2012-2016 Open Source Robotics Foundation
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Description:
# This script installs gazebo onto an Debian jessie system.



codename=`lsb_release -sc`

# Make sure we are running a valid Ubuntu distribution
if [ $codename != jessie ]; then
    echo "This script will only work on Debian jessie"
    exit 0
fi

## YARP

# Add the repository 
echo "Adding the gazebo repository"
if [ ! -e /etc/apt/sources.list.d/icub.list ]; then
    sudo sh -c "echo \"deb http://www.icub.org/debian ${codename} contrib/science\" > /etc/apt/sources.list.d/icub.list" 
fi

# Download the  keys - also ubuntu
has_key=$(sudo apt-key list | grep "Robotology Packages Maintainer")

echo "Downloading keys" 
if [ -z "$has_key" ]; then
    sudo gpg --keyserver pgpkeys.mit.edu --recv-key 57A5ACB6110576A6
    sudo gpg -a --export 57A5ACB6110576A6 | sudo apt-key add -
fi

# Update apt
echo "Retrieving packages"
sudo apt-get update -qq
echo "OK"

# Install yarp
if [ -z "$(dpkg -l| grep "\<yarp\>")" ]; then
    echo "Installing Gazebo"
    sudo apt-get --yes --force-yes install yarp

    echo "Complete."
    echo "Type yarpserver to start a yarp session."
fi    



echo
echo 
echo

## GAZEBO

# Add the repository
echo "Adding the gazebo repository"
if [ ! -e /etc/apt/sources.list.d/gazebo-stable.list ]; then
    sudo sh -c "echo \"deb http://packages.osrfoundation.org/gazebo/debian-stable ${codename} main\" > /etc/apt/sources.list.d/gazebo-stable.list" 
fi

# Download the OSRF keys
has_key=$(sudo apt-key list | grep "OSRF Repository")

echo "Downloading keys"
if [ -z "$has_key" ]; then
  wget --quiet http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
fi

 
# Update apt
echo "Retrieving packages"
sudo apt-get update -qq
echo "OK"

# Install gazebo
if [ -z "$(dpkg -l| grep "\<gazebo7\>")" ]; then
    echo "Installing Gazebo"
    sudo apt-get --yes --force-yes install gazebo7 libgazebo7-dev

    echo "Complete."
    echo "Type gazebo to start the simulator."
fi

OPT_DIR=${HOME}/opt

if ! [ -d ${OPT_DIR} ]; then
    mkdir ${OPT_DIR}
fi

# GAZEBO ICUB

cd ${OPT_DIR}
GAZEBO_DIR=${OPT_DIR}/icub-gazebo
if ! [ -d $GAZEBO_DIR ]; then  
    git clone https://github.com/robotology/icub-gazebo.git 
fi

GAZEBO_STRING="$(
cat << EOF   

#------------------------------------------------------------------
# GAZEBO ICUB

if [ -z "\$GAZEBO_MODEL_PATH" ]; then
    export GAZEBO_MODEL_PATH=${GAZEBO_DIR}
else
    export GAZEBO_MODEL_PATH=\${GAZEBO_MODEL_PATH}:${GAZEBO_DIR}
fi
#------------------------------------------------------------------

EOF
)"

if [ -z "$(cat ${HOME}/.bashrc | grep GAZEBO)" ]; then 
    echo "$GAZEBO_STRING" >> ${HOME}/.bashrc
fi


cd ${HOME}

## GAZEBO-YARP PLUGIN

GAZEBO_YARP=${OPT_DIR}/gazebo-yarp-plugins

if ! [ -d ${GAZEBO_YARP} ]; then
    cd /tmp
    git clone https://github.com/robotology/gazebo-yarp-plugins.git
    cd gazebo-yarp-plugins
    mkdir build 
    cd build
    cmake ../ -DCMAKE_INSTALL_PREFIX=${GAZEBO_YARP}
    make -j 4
    make install
fi


if [ -z "$(cat ${HOME}/.bashrc | grep GAZEBO_PLUGIN_PATH)" ]; then 
    echo >> ${HOME}/.bashrc
    echo "#------------------------------------------------------------------" >> ${HOME}/.bashrc
    echo "# GAZEBO YARP" >> ${HOME}/.bashrc
    echo >> ${HOME}/.bashrc
    echo "export GAZEBO_PLUGIN_PATH=\${GAZEBO_PLUGIN_PATH}:${GAZEBO_YARP}/lib" >> ${HOME}/.bashrc
    echo >> ${HOME}/.bashrc
    echo "#------------------------------------------------------------------" >> ${HOME}/.bashrc
fi

# GAZEBO WEB

curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install libjansson-dev nodejs libboost-dev imagemagick libtinyxml-dev mercurial cmake build-essential
curl -L https://www.npmjs.com/install.sh | sudo sh -
cd $HOME/opt; hg clone https://bitbucket.org/osrf/gzweb
cd $HOME/opt/gzweb
hg up gzweb_1.2.0
source /usr/share/gazebo/setup.sh
./deploy.sh -m local

